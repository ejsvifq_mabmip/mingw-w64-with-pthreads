This is a canadian compiled GCC native toolchain (multilib) with POSIX thread model. Built by the fast_io library's author

This contains C++ threads.

Unix Timestamp:1643965271.578729143
UTC:2022-02-04T09:01:11.578729143Z

fast_io:
https://github.com/tearosccebe/fast_io.git

build	:	x86_64-linux-gnu
host	:	x86_64-w64-mingw32
target	:	x86_64-w64-mingw32

g++ -v
Using built-in specs.
COLLECT_GCC=g++
COLLECT_LTO_WRAPPER=d:/x86_64-windows-gnu/x86_64-w64-mingw32-posix/x86_64-w64-mingw32/bin/../libexec/gcc/x86_64-w64-mingw32/12.0.1/lto-wrapper.exe
Target: x86_64-w64-mingw32
Configured with: ../../../../gcc/configure --disable-nls --disable-werror --disable-sjlj-exceptions --enable-threads=posix --target=x86_64-w64-mingw32 --prefix=/home/cqwrteur/toolchains/gnu/x86_64-w64-mingw32/x86_64-w64-mingw32-posix/x86_64-w64-mingw32 --disable-libstdcxx-verbose --enable-languages=c,c++ --with-gxx-libcxx-include-dir=/home/cqwrteur/toolchains/gnu/x86_64-w64-mingw32/x86_64-w64-mingw32-posix/x86_64-w64-mingw32/include/c++/v1 --enable-multilib --with-pkgversion=beijing2022winterolympics --host=x86_64-w64-mingw32
Thread model: posix
Supported LTO compression algorithms: zlib
gcc version 12.0.1 20220204 (experimental) (beijing2022winterolympics)
